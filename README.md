<img src="https://s3-us-west-2.amazonaws.com/imagesutils/profource-tecnologias-logo.gif"/>

# Partner Services 

<p align="center"><img src="https://s3-us-west-2.amazonaws.com/imagesutils/logo-interplace.png" width="700"/></p>

## 1: Descrição
A API de integração **Partner Services** tem como objetivo principal prover aos nossos parceiros uma maneira de integrar seus sistemas diretamente com a Interplace. Para isso fornecemos uma API RESTFull na qual o parceiro pode consultar seus pedidos, produtos, ajustar estoque e preço dos produtos e variações.

### 1.1: Como iniciar sua integração
O primeiro passo é solicitar suas credenciais de acesso para nosso time de integração, para isso envie um e-mail para igor.alexsander@pro4ce.com, este email deve conter alguns itens básicos como:
* Parceiro para qual está sendo desenvolvida a integração.
* Pessoa responsável pelo desenvolvimento.
* Se você for uma software house nos encaminhe o nome e site(caso possua).
* Arquitetura da solução (Web, Mobile, Desktop, etc.).
* Descrição sucinta do funcionamento da mesma.
<br />
Após uma breve análise destes dados criaremos suas credenciais em nosso ambiente de Sandbox.

### 1.2: Ambiente de Sandbox
No ambiente de sandbox o desenvolvedor pode exercitar todos os recursos da API sendo que todos os procedimentos são somente em ambiente de testes e nunca refletiram os ambientes reais, também cadastramos alguns 
produtos e pedidos fictícios assim o desenvolvedor pode se basear nos dados fictícios para gerar os seus.
Além da API de integração você terá acesso ao sistema administrativo no mesmo ambiente, sendo assim todas as rotinas que forem executadas via API poderão ser visualizadas em tempo real no Dashboard.
<br/>
Para ambos utilize as mesmas credenciais.
* URL API: https://sandbox.partnerservices.profourcetecnologias.com
* URL Dashboard: https://sandbox.interplaceapp.profourcetecnologias.com

### 1.3: Ambiente de Produção
* URL API: https://production.partnerservices.profourcetecnologias.com
* URL Dashboard: https://interplaceapp.profourcetecnologias.com

### 1.4: Documentação
Para facilitar o desenvolvimento de sua integração oferecemos o suporte necessário através de dois principais meios:

#### 1.4.1: Swagger
Acessando o link https://sandbox.partnerservices.profourcetecnologias.com/swagger-ui.html o navegador lhe solicitara um usuario e senha, informe suas credencias de teste, feito isso será exibida uma aplicação com todas as operações de nossa API, bastanto somente você informar os parametros e corpo das requisições.

<img src="https://s3-us-west-2.amazonaws.com/imagesutils/swagger-partnerservices.png" width="1000"/>

#### 1.4.2: Postman
Caso esteja familiarizado com o Postman abra o link abaixo que será aberto um projeto nele com todas as fucnionalidades da API. Para executar as requisições basta somente alterar as variaveis de usuario e senha.
<br />
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/e91403f6eb67483e7357#?env%5BPARTNERSERVICES%5D=W3sia2V5IjoiVVJMIiwidmFsdWUiOiJodHRwczovL3NhbmRib3gucGFydG5lcnNlcnZpY2VzLnByb2ZvdXJjZXRlY25vbG9naWFzLmNvbSIsImRlc2NyaXB0aW9uIjoiIiwidHlwZSI6InRleHQiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6InVzdWFyaW8iLCJ2YWx1ZSI6InVzdWFyaW8iLCJkZXNjcmlwdGlvbiI6IiIsInR5cGUiOiJ0ZXh0IiwiZW5hYmxlZCI6dHJ1ZX0seyJrZXkiOiJzZW5oYSIsInZhbHVlIjoic2VuaGEiLCJkZXNjcmlwdGlvbiI6IiIsInR5cGUiOiJ0ZXh0IiwiZW5hYmxlZCI6dHJ1ZX0seyJrZXkiOiJkYXRhYXR1YWwiLCJ2YWx1ZSI6IiIsImRlc2NyaXB0aW9uIjoiIiwidHlwZSI6InRleHQiLCJlbmFibGVkIjp0cnVlfV0=)

## 2: Autenticação
Utilizamos o meio de autenticação **Basic Auth** para autenticar e autorizar a comunicação do parceiro com nossa API. Em qualquer uma das requisições é necessário enviar
um Http Header **Authorization** no seguinte formato:

``` 
//Formato
usuario:senha

//Após encodar para Base 64
dXN1YXJpbzpzZW5oYQ==

//Header final
Authorization: Basic dXN1YXJpbzpzZW5oYQ==

```

## 3: Gestão de pedidos
Através dos métodos abaixo o parceiro consegue fazer uma gestão completa de seus pedidos. O fluxo dos pedidos se inicia quando um cliente compra um produto do parceiro em qualquer um dos canais,
o status inicial do pedido pode variar entre **Aguardando Verificação** e **A Enviar**. O status aguardando verificação acontece quando o cliente opta por pagamento com aprovação posterior como Boleto,
sendo assim o pedido fica neste status até o pagamento ser aprovado ou reprovado, em ambos os casos o pedido voltará para fila de integração. Já o status a enviar acontece na maioria dos casos, pois o pagamento é 
processado na mesma hora.
<br/>
Utilizamos um mecanismo de fila para disponibilizar os pedidos para o parceiro, onde toda situação que o pedido mudar seu status e este seja de responsabilidade da Interplace o colocamos na fila de integração
para que o parceiro possa incluí-lo ou atualizá-lo em seu ERP. Contamos também que você parceiro remova este pedido da nossa fila, pois assim deixamos de usar recursos computacionais desnecessariamente.

### 3.1: Situações de pedidos
Um pedido pode variar para qualquer um dos status abaixo, visto que uma vez alterado o status de um pedido ele não pode mais retornar para o anterior.
* (I) 2 - Aguardando verificação. Neste status o parceiro não deve fazer nada com esse pedido, pois ele ainda não teve pagamento aprovado.
* (I) 11 - A Enviar: Neste status o parceiro já pode dar início ao processamento desse pedido.
* (I) 9 - Cancelado pelo cliente: Este status ocorre quando o cliente desiste da compra.
* (I) 10 - Cancelado pela loja: Este status ocorre quando o time responsável pelos pedidos na interplace cancelar manualmente o mesmo por fatores variáveis· 
* (P) 14 - Faturado: Status para qual o pedido vai quando o parceiro nos envia os dados de faturamento.
* (P) 12 - Enviado: Status para qual o pedido vai quando o parceiro nos envia os dados de envio.
* (P) 13 - Finalizado: Status para qual o pedido vai quando o parceiro nos atualiza o pedido para finalizado.
<br />Responsabilidade do status:
***(I) Interplace (P) Parceiro***


### 3.2: JSON completo de um pedido
```
{
   "numero": "00000000",
   "protocolo": "778e219526a5230ccc5856049d9cde7c",
   "dataCompra": "2018-10-30T02:54:12.000+0000",
   "dataAprovacao": "2018-10-30T02:54:12.000+0000",
   "identificadorPlataforma": "0000000",
   "identificadorMarketplace": "000000000000",
   "loja": "INTERPLACESHOP",
   "origem": "LOJASAMERICANAS",
   "cliente": {
      "cpfCnpj": "00000000000",
      "nome": "TONY STARK",
      "inscricaoEstadual": null,
      "endereco": {
         "logradouro": "AVENIDA DO ARTISTAS",
         "numero": "9065",
         "complemento": "COMPLEMENTO INFORMADO POR ELE",
         "bairro": "MALIBU POINT",
         "cep": "00000000",
         "cidade": {
            "codigo": 3550308,
            "nome": "HOLLYWOOD",
            "codigoEstado": 35,
            "descricaoEstado": "CALIFORNIA",
            "uf": "SP"
         }
      },
      "telefone": "11994694554",
      "tipoCliente": "PF"
   },
   "enderecoEntrega": {
      "nomeRecebedor": "Happy Hogan",
      "logradouro": "AVENIDA DO ARTISTAS",
      "numero": "9065",
      "bairro": "MALIBU POINT",
      "cep": "00000000",
      "cidade": "HOLLYWOOD",
      "estado": "SP"
   },
   "itens": [
      {
         "produtoPedido": {
            "codigoInterplace": "000003-000002",
            "codigoParceiro": "CAP-003-001",
            "nome": "CAPACETE HOMEM DE FERRO",
            "atributoVariacao": "AZUL"
         },
         "quantidade": 1,
         "unitario": 100,
         "total": 100
      }
   ],
   "totalBruto": 110,
   "frete": 10,
   "desconto": 0,
   "comissao": 16,
   "url": "https://interplaceapp.profourcetecnologias.com/pedidocompleto/00000000",
   "descricaoStatus": "A enviar",
   "total": 110,
   "codigoStatus": 11
}
```

### 3.3: Obter pedidos disponiveis
Para obter a listagem de pedidos disponíveis, esta requisição retorna um Array de **Pedido** com todos os pedidos novos bem como os que sofreram atualizações de status.
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/pedidos/fila
 * Method: GET

### 3.4: Remover um pedido da fila
Após consumir cada pedido da fila e integrar em seu sistema você deve remover o mesmo da fila de integração.
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/pedidos/fila/{protocolo}
 * Method: DELETE

### 3.5: Faturar pedido
Para enviar os dados de faturamento de um pedido:
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/pedidos/faturar/{numero_do_pedido}
 * Method: PUT
 * Body: 
```
    {
    	"numeroNota":"000000",
    	"serieNota":"000",
    	"emissaoNota":"2018-10-30T02:54:12.000+0000",
    	"chaveNota":"411811000000000000000550010000048241212479239"
    }

```

### 3.6: Enviar pedido
Para enviar os dados de envio de um pedido:
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/pedidos/enviar/{numero_do_pedido}
 * Method: PUT
 * Body: 
```
    {
    	"nomeTransportadora":"CORREIOS",
    	"codigoRastreio":"PB00000000096",
    	"dataPostagem": "2018-10-30T02:54:12.000+0000"
    }
```

### 3.7: Finalizar pedido
Quando um pedido for entregue é de sua responsabilidade atualizar o pedido para finalizado.
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/pedidos/finalizar/{numero_do_pedido}
 * Method: PUT
 * Body: Está requisição não irá possuir corpo.

## 4: Gestão de produtos
Nosso cadastro de produtos é baseado no mecanismo de produto/variação onde um produto pode ser individual, como por exemplo um ***Jogo de copos 6PCs*** como também o mesmo pode possuir variações, 
por exemplo ***Batedeira Cadence*** onde a mesma pode possuir variações de Voltagem 110V 220V.
Uma particularidade importante é que o estoque do produto é separado do produto em si, ou seja, a gestão do estoque ocorre em endpoints separados do produto por motivo de que trabalhamos 
com um mecanismo de crédito e débito de estoque, assim temos um historico completo de quaisquer mudanças que ocorram no ciclo de vida do estoque.

### 4.1: JSON completo de um produto sem variações

```
    {
        "codigo": "000032",
        "codigoParceiro": "0001",
        "nome": "Armadura homem de ferro completa",
        "descricao": "Armadura do homem de ferro em metal leve.",
        "ean": "7898466527475",
        "preco": 2500,
        "marca": "Marvel",
        "peso": 170.0,
        "altura": 190,
        "largura": 45,
        "comprimento": 45,
        "crossdocking": 10,
        "modelo": "Mark 42"
    }
``` 

### 4.2: JSON completo de um produto com variações
```
    {
        "codigoParceiro": "003",
        "nome": "CAPACETE HOMEM DE FERRO",
        "descricao": "Capacete do homem de ferro em diversas cores",
        "tipoVariacao": "Cor",
        "marca": "Marvel",
        "peso": 3.4,
        "altura": 25,
        "largura": 45,
        "comprimento": 52,
        "crossdocking": 3,
        "modelo": "Mark-42",
        "variacoes": [
            {
                "codigoParceiro": "003-001",
                "ean": "7898466532073",
                "atributoVariacao": "Azul",
                "preco": 374.1
            },
            {
                "codigoParceiro": "003-002",
                "ean": "7898466532080",
                "atributoVariacao": "Vermelho",
                "preco": 374.2
            }
        ]
    }
```
### 4.3: JSON completo de um produto com variações e subvariações
Além de o produto possuir variações as proprias variações podem possuir variações, abaixo esta um exemplo para retratar isso.

```
{
    "codigoParceiro": "000040",
    "nome": "Cama para Cachorro Toca Pop Narciso Pet Diversas Cores",
    "tipoVariacao": "Cor",
    "marca": "Narciso",
    "peso": 0.3,
    "altura": 10,
    "largura": 10,
    "comprimento": 10,
    "crossdocking": 3,
    "modelo": "Cama Toca Pop",
    "variacoes": [
        {
            "codigoParceiro": "000040-000002",
            "tipoVariacao": "Tamanho",
            "atributoVariacao": "Roxo",
            "variacoes": [
                {
                    "codigoParceiro": "000040-000002-000002",
                    "ean": "1111111111111",
                    "atributoVariacao": "GG",
                    "preco": 49.9
                },
                {
                    "codigoParceiro": "000040-000002-000001",
                    "ean": "2222222222222",
                    "atributoVariacao": "G",
                    "preco": 43.9
                }
            ]
        },
        {
            "codigoParceiro": "000040-000001",
            "tipoVariacao": "Tamanho",
            "atributoVariacao": "Azul",
            "variacoes": [
                {
                    "codigoParceiro": "000040-000001-000001",
                    "ean": "3333333333333",
                    "atributoVariacao": "G",
                    "preco": 43.9
                },
                {
                    "codigoParceiro": "000040-000001-000002",
                    "ean": "4444444444444",
                    "atributoVariacao": "GG",
                    "preco": 49.9
                }
            ]
        },
        {
            "codigoParceiro": "000040-000003",
            "tipoVariacao": "Tamanho",
            "atributoVariacao": "Vermelho",
            "variacoes": [
                {
                    "codigoParceiro": "000040-000003-000001",
                    "ean": "5555555555555",
                    "atributoVariacao": "G",
                    "preco": 43.9
                },
                {
                    "codigoParceiro": "000040-000003-000002",
                    "ean": "6666666666666",
                    "atributoVariacao": "GG",
                    "preco": 49.9
                }
            ]
        }
    ]
}
```


Algumas particularidades relacionadas as variações:
* Dados comuns (largura, altura, comprimento, peso, crossdocking) caso não informados nas variações assumirão o valor que foi informado no pai mais caso precise diferenciar somente informar o atributo na variação com o valor desejado.
* Os tipos de variações bem como seus possiveis valores são estaticos pré definidos pela interplace, somente sofrem mudanças caso necessário.

### 4.4: Tipos de variações e atributos
Ainda não contamos com endpoints na API para que o parceiro busque estas informações, então abaixo enumeramos todos os tipos e valores possiveis atualmente.
* Voltagem
    - 110V
    - 220V
* Cor
    - Azul
    - Vermelho
    - Amarelo
    - Verde
    - Roxo
    - Rosa
* Tamanho
    - P
    - M
    - G
    - GG
    - 80cm
    - 90cm

### 4.5: Incluir ou alterar um produto
Caso o produto com o código do parceiro informado já exista sera efetuada uma atualização do mesmo, caso não será cadastrado um novo.
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/produtos
 * Method: POST
 * Body: Objeto produto individual, com variações ou com subvariaões.

### 4.6: Obter um produto
Caso o parceiro precise consultar seus produtos em nossos sistemas basta realizar uma requisição no formato abaixo:
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/produtos/{codigo_do_produto_ou_variação}
 * Method: POST
 * Retorno: Objeto produto individual, com variações ou com subvariaões.

## 5: Gestão de estoque
Como citado anteriormente o fluxo de estoque é separado do produto. 

### 5.1: Ajuste de estoque
Este metodo faz um ajuste positivo ou negativo do estoque atual de um produto/variação, porém esse número deve ser a quantidade que o parceiro deseja incluir ou retirar do estoque pois somaremos ou subrairemos esse numero do estoque atual.
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/produtos/estoque
 * Method: PUT
 * Body: 
```
    {
    	"quantidade": 10,
    	"codigoProduto": "SEB-220"
    }
```

### 5.2: Obter estoque atual
Este método irá retornar a quantidade atual de estoque do produto/variacao
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/produtos/estoque/{codigo_do_produto_variação}
 * Method: GET
 * Retorno: 
```
    {
        "quantidade": 30,
        "codigoProduto": "{codigo_do_produto_variação}"
    }
```

### 5.3: Atualizar preço
Este método atualiza o preço do produto/variação com o código solicitado, deve-se atentar que toda aatualização de preços devem ocorrer no ultimo nivel das variações caso o produto possua.
 * Endpoint: https://sandbox.partnerservices.profourcetecnologias.com/produtos/preco
 * Method: PUT
 * Body: 
```
    {
    	"codigoProduto":"000037",
    	"preco": 3800
    }
```

## Considerações finais
Estamos a disposição para resolver quaisquer tipos de dificuldades que você desenvolvedor venha ter, além disso estamos abertos a sugestões e melhorias em nossos serviços, caso deseje alguma informação que não conste 
nos objetos nos encaminhe que analisaremos e caso concluirmos que agregará valor para todos os parceiros, o mais breve possível implementaremos a demanda.
Também gostaríamos de deixar claro que estamos em melhoria contínua, ou seja, esta documentação não é estática e pode vir a mudar conforme as necessidades, mais fique tranquilo faremos o possível para manter
retrocompatibilidade entre as versões e qualquer mudança que venha ocorrer o comunicaremos.





